"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gCOLUMNS_TABLE_ORDER = [
    "orderid",
    "kichco",
    "loaipizza",
    "nuocuong",
    "thanhtien",
    "hoten",
    "trangthai",
    "sodienthoai",
    "action",
];
// khai báo hằng số toàn cục cho column table
const gCOL_ORDERID = 0;
const gCOL_KICHCO = 1;
const gCOL_LOAIPIZZA = 2;
const gCOL_NUOCUONG = 3;
const gCOL_THANHTIEN = 4;
const gCOL_HOTEN = 5;
const gCOL_TRANGTHAI = 6;
const gCOL_SODIENTHOAI = 7;
const gCOL_ACTION = 8;
var gOrderTable = $("#orders-table").DataTable({
    columns: [
        { data: gCOLUMNS_TABLE_ORDER[gCOL_ORDERID] },
        { data: gCOLUMNS_TABLE_ORDER[gCOL_KICHCO] },
        { data: gCOLUMNS_TABLE_ORDER[gCOL_LOAIPIZZA] },
        { data: gCOLUMNS_TABLE_ORDER[gCOL_NUOCUONG] },
        { data: gCOLUMNS_TABLE_ORDER[gCOL_THANHTIEN] },
        { data: gCOLUMNS_TABLE_ORDER[gCOL_HOTEN] },
        { data: gCOLUMNS_TABLE_ORDER[gCOL_TRANGTHAI] },
        { data: gCOLUMNS_TABLE_ORDER[gCOL_SODIENTHOAI] },
        { data: gCOLUMNS_TABLE_ORDER[gCOL_ACTION] },
    ],
    columnDefs: [
        {
        targets: gCOL_ACTION,
        defaultContent:
            '<button class="btn btn-success btn-detail">Chi Tiết</button>',
        },
    ],
});
var gOrderDataTable = [];
var gOrderID = ''
var gID = ''
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
$(document).ready(function () {
    callApiLoadAllOrderToTable();
    $("#orders-table").on("click", ".btn-detail", function () {
        onBtnDetailDataOrderClick(this);
    });
    $('.btn-confirmed').on('click', function(){
        onBtnConfirmedStatusClick()
    })
    $('.btn-cancle').on('click', function(){
        onBtnCancleStatusClick()
    })
});
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
function onBtnCancleStatusClick(){
    var vObjectRequest = {
        trangThai: "cancle" //3: trang thai open, confirmed, cancel tùy tình huống
    }
    $.ajax({
        type: "PUT",
        url: "http://203.171.20.210:8080/devcamp-pizza365/orders/"+gID,
        contentType: 'application/json',
        data: JSON.stringify(vObjectRequest),
        dataType: "json",
        success: function (response) {
            $('#orders-modal').modal('hide')
            callApiLoadAllOrderToTable()
        }
    });
}
//ham xử lý sự kiện thay đổi trạng thái confirmed
function onBtnConfirmedStatusClick(){
    var vObjectRequest = {
        trangThai: "confirmed" //3: trang thai open, confirmed, cancel tùy tình huống
    }
    $.ajax({
        type: "PUT",
        url: "http://203.171.20.210:8080/devcamp-pizza365/orders/"+gID,
        contentType: 'application/json',
        data: JSON.stringify(vObjectRequest),
        dataType: "json",
        success: function (response) {
            $('#orders-modal').modal('hide')
            callApiLoadAllOrderToTable()
        }
    });
}
function getDataOrderFromButton(paramButton) {
    var vTableRow = $(paramButton).parents("tr");
    var vUserRowData = gOrderTable.row(vTableRow).data();
    return vUserRowData;
}
function onBtnDetailDataOrderClick(paramElement) {
    var vDataOrder = getDataOrderFromButton(paramElement);
    gOrderID = vDataOrder.orderid;
    console.log(vDataOrder);
    $('#orders-modal').modal('show')
    callApiGetDataOrderByOrderId(gOrderID)
}
function callApiGetDataOrderByOrderId(paramOrderID){
    $.ajax({
        type: "GET",
        url: "http://203.171.20.210:8080/devcamp-pizza365/orders/"+paramOrderID,
        dataType: "json",
        success: function (response) {
            console.log(response)
            gID = response.id;
            displayDataResponseToElementForm(response)
        }
    });
}
function displayDataResponseToElementForm(paramResponse){
    $('#inp-order-id').val(paramResponse.orderId);
    $('#select-size-combo').val(paramResponse.kichCo);
    $('#inp-duong-kinh').val(paramResponse.duongKinh);
    $('#inp-suon-nuong').val(paramResponse.suon);
    $('#inp-do-uong').val(paramResponse.idLoaiNuocUong);
    $('#inp-number-drink').val(paramResponse.soLuongNuoc);
    $('#inp-voucher-id').val(paramResponse.idVourcher);
    $('#inp-pizza-type').val(paramResponse.loaiPizza);
    $('#inp-salad').val(paramResponse.salad);
    $('#inp-thanh-tien').val(paramResponse.thanhTien);
    $('#inp-giam-gia').val(paramResponse.giamGia);
    $('#inp-fullname').val(paramResponse.hoTen);
    $('#inp-email').val(paramResponse.email);
    $('#inp-phone-number').val(paramResponse.soDienThoai);
    $('#inp-address').val(paramResponse.diaChi);
    $('#inp-message').val(paramResponse.loiNhan);
    $('#inp-trang-thai').val(paramResponse.trangThai);
    $('#inp-ngay-tao').val(paramResponse.ngayTao);
    $('#inp-ngay-update').val(paramResponse.ngayCapNhat);
}
function callApiLoadAllOrderToTable() {
    $.ajax({
        type: "GET",
        url: "http://203.171.20.210:8080/devcamp-pizza365/orders",
        dataType: "json",
        success: function (response) {
        mountDataToVariableGloble(response);
        loadDataTable(gOrderTable, gOrderDataTable);
        },
    });
}
function mountDataToVariableGloble(paramRes) {
    gOrderDataTable = []
    for (var bIndex in paramRes) {
        var bOrder = {
        orderid: paramRes[bIndex].orderId,
        kichco: paramRes[bIndex].kichCo,
        loaipizza: paramRes[bIndex].loaiPizza,
        nuocuong: paramRes[bIndex].idLoaiNuocUong,
        thanhtien: paramRes[bIndex].thanhTien,
        hoten: paramRes[bIndex].hoTen,
        trangthai: paramRes[bIndex].trangThai,
        sodienthoai: paramRes[bIndex].soDienThoai,
        };
        gOrderDataTable.push(bOrder);
    }
}
function loadDataTable(paramTable, paramData) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    paramTable.clear();
    //Cập nhật data cho bảng
    paramTable.rows.add(paramData);
    //Cập nhật lại giao diện hiển thị bảng
    paramTable.draw();
}